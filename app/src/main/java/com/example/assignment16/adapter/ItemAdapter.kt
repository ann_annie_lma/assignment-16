package com.example.assignment16.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.assignment16.R
import com.example.assignment16.databinding.DatabaseItemBinding
import com.example.assignment16.entity.Item

class ItemAdapter(private val onItemClicked: (position: Int) -> Unit) : RecyclerView.Adapter<ItemAdapter.ListViewHolder>() {

    private var itemList = listOf<Item>()

    inner class ListViewHolder(val binding: DatabaseItemBinding,private val onItemClicked: (position: Int) -> Unit) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener
    {
        fun bind(item: Item) {
            binding.title.text = item.title
            binding.tvDescription.text = item.description

            Glide.with(binding.ivIcon.getContext())  // glide with this view
                .load(item.imageUrl) // set this size
                .placeholder(R.mipmap.ic_launcher) // default placeHolder image
                .error(R.mipmap.ic_launcher)
                .centerCrop()// use this image if faile
                .into(binding.ivIcon)

        }

        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            val position = adapterPosition
            onItemClicked(position)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val viewHolder = ListViewHolder(
            DatabaseItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),onItemClicked
        )

        return viewHolder
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val currentItem = itemList[position]
        holder.bind(currentItem)


    }

    override fun getItemCount(): Int = itemList.size


    fun setData(user:List<Item>)
    {
        this.itemList = user
        notifyDataSetChanged()
    }


}