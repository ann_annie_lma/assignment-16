package com.example.assignment16.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assignment16.R
import com.example.assignment16.adapter.ItemAdapter
import com.example.assignment16.databinding.FragmentItemListBinding
import com.example.assignment16.entity.Item
import com.example.assignment16.viewmodel.ItemViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect

class ItemListFragment : Fragment() {

    private lateinit var itemViewModel : ItemViewModel

    private lateinit var listAdapter : ItemAdapter

    private var itemList : List<Item> = listOf()


    private var _binding: FragmentItemListBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentItemListBinding.inflate(inflater, container, false)

        setHasOptionsMenu(true)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
        setUprecycler()
        observers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun listeners()
    {
        binding.btnAddNewItem.setOnClickListener {
            val action = ItemListFragmentDirections.actionItemListFragmentToAddFragment()
            findNavController().navigate(action)
        }
    }

    private fun setUprecycler()
    {
        listAdapter = ItemAdapter { position -> onListItemClick(position) }
        binding.rvItem.layoutManager = LinearLayoutManager(requireContext())
        binding.rvItem.adapter = listAdapter
    }

    private fun observers(){
       itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)


        lifecycleScope.launch {
                itemViewModel.readAllData.collect()
                {
                   if(it.isEmpty())
                   {
                       binding.tvInfoAboutItem.setText(R.string.inof_about_items)
                   } else
                   {
                       listAdapter.setData(it)
                       itemList = it
                   }
                }
            }

    }

    private fun onListItemClick(position:Int)
    {
        val action = ItemListFragmentDirections.actionItemListFragmentToUpdateFragment(itemList[position])
        findNavController().navigate(action)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_item_menu,menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.delete_all)
        {
            deleteAllItems()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun  deleteAllItems()
    {
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setPositiveButton("Yes") {_,_ ->
            itemViewModel.deleteAllItems()
            Snackbar.make(binding.root,"All Items removed", Snackbar.LENGTH_SHORT)
                .show()

        }

        dialogBuilder.setNegativeButton("No") {_,_ ->}

        dialogBuilder.setTitle("Delete all items?")
        dialogBuilder.setMessage("Are us sure u want to delete all items?")
        dialogBuilder.create().show()
    }
}