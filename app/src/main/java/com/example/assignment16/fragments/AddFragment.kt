package com.example.assignment16.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.assignment16.R
import com.example.assignment16.databinding.FragmentAddBinding
import com.example.assignment16.databinding.FragmentItemListBinding
import com.example.assignment16.entity.Item
import com.example.assignment16.viewmodel.ItemViewModel
import com.google.android.material.snackbar.Snackbar

class AddFragment : Fragment() {

    private lateinit var mUserViewModel : ItemViewModel

    private var _binding: FragmentAddBinding? = null

    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddBinding.inflate(inflater, container, false)

        //init viewmodel
        mUserViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun listeners()
    {
        binding.btnAdd.setOnClickListener {
           addNewItem()
        }
    }

    private fun addNewItem()
    {
        val title = binding.tvAddTitle.text.toString()
        val description = binding.tvAddDescription.text.toString()
        val image = binding.tvAddImage.text.toString()


        if(inputValidation(title,description))
        {
            val item = Item(0,title,description,image)
            mUserViewModel.addItem(item)
            Snackbar.make(binding.root,"Successfully Inserted", Snackbar.LENGTH_SHORT)
                .show()

            val action = AddFragmentDirections.actionAddFragmentToItemListFragment()
            findNavController().navigate(action)

        }
    }



    private fun inputValidation(title:String,description:String) : Boolean
    {
        var result = true
       if(title.isNotEmpty() && title.length <5 || title.length >30) {
           Snackbar.make(
               binding.root,
               "Title should be between[5,30] symbols",
               Snackbar.LENGTH_SHORT
           )
               .show()
           result = false
       }
        if(description.isNotEmpty()&& description.length < 32 || description.length > 300)
       {
           Snackbar.make(
               binding.root,
               "Description should be between [32,300] symbols",
               Snackbar.LENGTH_SHORT
           )
               .show()
           result = false
       }

        return result

    }
}