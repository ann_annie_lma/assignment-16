package com.example.assignment16.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.assignment16.R
import com.example.assignment16.databinding.FragmentItemListBinding
import com.example.assignment16.databinding.FragmentUpdateBinding
import com.example.assignment16.entity.Item
import com.example.assignment16.viewmodel.ItemViewModel
import com.google.android.material.snackbar.Snackbar


class UpdateFragment : Fragment() {

    private lateinit var itemViewModel : ItemViewModel

    private val args by navArgs<UpdateFragmentArgs>()

    private var _binding: FragmentUpdateBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUpdateBinding.inflate(inflater, container, false)

        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpdateFields()
        listeners()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setUpdateFields()
    {
        binding.apply {
            tvUpdateTitle.setText(args.item.title)
            tvUpdateDescription.setText(args.item.description)
            tvUpdateImage.setText(args.item.imageUrl)
        }
    }

    private fun listeners()
    {
        binding.btnUpdate.setOnClickListener {
            updateItem()
        }
    }

    private fun updateItem()
    {
        val title = binding.tvUpdateTitle.text.toString()
        val description = binding.tvUpdateDescription.text.toString()
        val url = binding.tvUpdateImage.text.toString()

        if(inputCheck(title,description))
        {
            val updateItem = Item(args.item.id,title,description,url)

            itemViewModel.updateItem(updateItem)

            Snackbar.make(binding.root,"Updated Successfully", Snackbar.LENGTH_SHORT)
                .show()

            val action = UpdateFragmentDirections.actionUpdateFragmentToItemListFragment()
            findNavController().navigate(action)
        }
        else
        {
            Snackbar.make(binding.root,"Please fill all fields", Snackbar.LENGTH_SHORT)
                .show()
        }
    }


    private fun inputCheck(firstName:String,lastName:String) : Boolean
    {
        return !(TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName))
    }
}