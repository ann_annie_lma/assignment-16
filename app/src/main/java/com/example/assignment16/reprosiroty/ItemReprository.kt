package com.example.assignment16.reprosiroty

import com.example.assignment16.entity.Item
import com.example.assignment16.database.ItemDao
import kotlinx.coroutines.flow.Flow

class ItemReprository(private val userDao : ItemDao)  {


    val readAllData : Flow<List<Item>> = userDao.readAllData()

        suspend fun addUser(user: Item)
        {
            userDao.addUser(user)
        }

        suspend fun updateUser(user:Item)
        {
            userDao.updateUser(user)
        }

        suspend fun deleteAllItems()
        {
            userDao.deleteAllItems()
        }
}