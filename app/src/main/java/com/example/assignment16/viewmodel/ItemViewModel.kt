package com.example.assignment16.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.example.assignment16.entity.Item
import com.example.assignment16.database.ItemDatabase
import com.example.assignment16.reprosiroty.ItemReprository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class ItemViewModel(application:Application) : AndroidViewModel(application) {

    val readAllData : Flow<List<Item>>
    private val reprository : ItemReprository

    init {
        val userDao = ItemDatabase.getDataBase(application).itemDao()
        reprository = ItemReprository(userDao)
        readAllData = reprository.readAllData
    }

    fun addItem(user: Item)
    {
        viewModelScope.launch {
            reprository.addUser(user)
        }
    }

    fun updateItem(user:Item)
    {
        viewModelScope.launch {
            reprository.updateUser(user)
        }
    }

    fun deleteAllItems()
    {
        viewModelScope.launch {
            reprository.deleteAllItems()
        }

    }
}