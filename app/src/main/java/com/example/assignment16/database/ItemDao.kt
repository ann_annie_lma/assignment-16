package com.example.assignment16.database

import androidx.room.*
import com.example.assignment16.entity.Item
import kotlinx.coroutines.flow.Flow

@Dao
interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addUser(user: Item)

    @Query("SELECT * FROM ITEM_TABLE")
    fun readAllData() : Flow<List<Item>>

    @Update
    suspend fun updateUser(user:Item)

   @Query("DELETE FROM ITEM_TABLE")
   suspend fun deleteAllItems()


}