package com.example.assignment16.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.assignment16.entity.Item

@Database(entities = [Item::class],version = 1,exportSchema = false)
abstract class ItemDatabase : RoomDatabase()
{
    abstract fun itemDao() : ItemDao

    companion object
    {
        private var INSTANCE:ItemDatabase? = null

        fun getDataBase(context: Context) : ItemDatabase
        {
            val tempInstance = INSTANCE
            if(tempInstance != null)
            {
                return tempInstance
            }

            synchronized(this)
            {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ItemDatabase::class.java,
                    "user_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }

}